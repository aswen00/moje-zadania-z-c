#include <stdio.h>
#include <stdlib.h>

typedef enum {
	klasa1=30,
	klasa2,
	klasa3=25,
	klasa4,
	klasa5=20
}rocznik;

int wypisz()
{
	printf("%d \n",klasa1);
	printf("%d \n",klasa2);
	printf("%d \n",klasa3);
	printf("%d \n",klasa4);
	printf("%d \n",klasa5);
	
	int suma = klasa1+klasa2+klasa3+klasa4+klasa5;
	return suma;
}

int zwroc()
{
	rocznik pierwsza = klasa1;
	return pierwsza;
}


int main() {
	printf("\n Suma uczniow we wszystkich klasach: %d",wypisz());
	printf("\n Zwracam: %d", zwroc());
	return 0;
}
