#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main() {
	
	float battery_voltage = 100.4633232;
	printf("Pierwotna wartość napięcia baterii: %f\n", battery_voltage);

	uint16_t bat_voltage = battery_voltage * 100;
	printf("Wartosc jako 2-bajtowa liczba calkowita %d\n", bat_voltage);

	uint8_t byte_0 = bat_voltage;
	uint8_t byte_1 = bat_voltage >> 8;

	printf("Wartosc 1 bajta: %d\nWartosc 2 bajta: %d\n",byte_0, byte_1);
	
	uint16_t received_bat_voltage =byte_0 + (byte_1 << 8);
	printf("Odebrana liczba calkowita: %d\n",received_bat_voltage);
	float voltage_ready_for_app = received_bat_voltage / 100.0;
	printf("Liczba do wyslania: %f\n",voltage_ready_for_app);


	float predkosc = 94.3216;
	float current = 143.3213;
	float odometer = 7875.3214;

	uint8_t predkosca = predkosc;
	byte_0 = predkosca;	
	printf("Wartosc 1 bajta: %d\n",byte_0);	
	uint8_t received_predkosc = byte_0;
	float predkosc_for_app = received_predkosc;
	printf("Predkosc do wyslania: %f\n",predkosc_for_app);
	
	
	uint16_t currenta = current * 100;
	printf("Odebrana liczba calkowita: %d\n",currenta);
	byte_0 = currenta;
	byte_1 = currenta >>8;
	uint16_t received_current = byte_0 + (byte_1 << 8);
	float current_for_app = received_current / 100.0;
	printf("Predkosc do wyslania: %f\n",current_for_app);	
	
	uint32_t odometera = odometer*100;
	printf("Odebrana liczba calkowita: %d\n",odometera);
	byte_0 = odometera;
	byte_1 = odometera >> 8;
	uint8_t byte_2 = odometera >> 16;
	printf("Wartosc 1 bajta: %d\nWartosc 2 bajta: %d\nWartosc 3 bajta: %d\n",byte_0,byte_1,byte_2);	
	uint16_t received_odometer = byte_0 + (byte_1 >> 8) + (byte_2 >> 16);
	printf("WARTOSC!!!: %d\n",received_odometer);
	float odometer_for_app = received_odometer / 100.0;
	printf("Odometer do wyslania: %f\n",odometer_for_app);	
	
	
	return 0;
}