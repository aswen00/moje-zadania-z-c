#include <stdio.h>
#include <stdlib.h>

typedef struct Stolica
{
	char *nazwa;
	char *panstwo;
	float ludnosc;
	
}stolica, *ws;


// zakladamy np,ze w ciagu roku ludnosc stolicy wzrosla o 5%
void aktualizacja(stolica *ws)
{
	printf("ludnosc po aktualizacji: %.0f\n",ws -> ludnosc*1.05);
}
//chcialem dodac jeszcze na poczatku cos w stylu 	printf("%s ludnosc[...] \n, stolica.nazwa [...]) zeby bylo wiadomo w ktorej dokladnie stolicy owy wynik jest aktualizowany, ale nie wiem jak do konca do zrobic :c


int main() {
	
	stolica lublana, moskwa, zagrzeb;
	lublana.nazwa="Lublana";
	lublana.panstwo="Slowenia";
	lublana.ludnosc=290000;
	
	moskwa.nazwa="Moskwa";
	moskwa.panstwo="Rosja";
	moskwa.ludnosc=11920000;
	
	zagrzeb.nazwa="Zagrzeb";
	zagrzeb.panstwo="Chorwacja";
	zagrzeb.ludnosc=806000;
	
	printf("%s \nStolica panstwa: %s, ludnosc: %.0f \n\n",lublana.nazwa,lublana.panstwo,lublana.ludnosc);
	printf("%s \nStolica panstwa: %s, ludnosc: %.0f \n\n",moskwa.nazwa,moskwa.panstwo,moskwa.ludnosc);
	printf("%s \nStolica panstwa: %s, ludnosc: %.0f \n\n",zagrzeb.nazwa,zagrzeb.panstwo,zagrzeb.ludnosc);
	
	
	aktualizacja(&lublana);
	aktualizacja(&moskwa);
	aktualizacja(&zagrzeb);
	
	return 0;
}
